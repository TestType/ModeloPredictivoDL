from proyectornn import modulo_datos as md
from proyectornn import modulo_modelo as mm
from proyectornn import modulo_graficas as mg
import numpy as np

#data = md.input_datos_request('http://192.168.0.17:54286/api/DataTraining?Filtro=DatosPorDias&FechaIni=2016-01-01&FechaFin=2017-12-31')
data = md.input_datos_json('./proyectornn/data.json')
window_size = 14
data = md.limpiar_outliers(data)
data = data.cantidad_servicios.values


#Transormacion de vectores a matriz de datos
matrix = np.reshape(data, (data.shape[0], 1))

# Escalado de datos (normalizacion)
data_normalizada, escala = md.normalizar_datos('robust', matrix)
#data_normalizada, escala = md.normalizar_datos('power', matrix)
#data_normalizada, escala = md.normalizar_datos('quantile', matrix)
#data_normalizada, escala = md.normalizar_datos('minmax', matrix)

# Construye la ventana de datos
raw = md.crear_ventana(data_normalizada, window_size,1)

# division de los datos
x_metric, y_metric = md.dividir_datos(raw)

#transformacion de matriz a cubos de datos
x_metric = np.reshape(x_metric, (x_metric.shape[0], x_metric.shape[1], 1))

# Construccion del modelo predictivo (input_time_step, input_dim, hidden_unit, dropout)
hidden_unit = 50
dropout =  0.2
model = mm.construir_modelo(x_metric.shape[1], x_metric.shape[2], hidden_unit, dropout)

# Entrenamiento del modelo con el set de datos destinado para este fin
batch_size = 2
epochs = 50
validation = 0.2
history, model = mm.entrenar_modelo_metricas(model, x_metric, y_metric, batch_size, validation, epochs)

# grafica la perdida de datos en el entrenamiento y la validacion
mg.graficar_perdida_datos(history)
