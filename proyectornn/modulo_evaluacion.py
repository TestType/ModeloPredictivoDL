from sklearn.metrics import explained_variance_score,  mean_absolute_error, mean_squared_error, mean_squared_log_error, median_absolute_error, r2_score
from math import sqrt
import numpy as np

def generar_metricas(real, prediccion):
    varianza = explained_variance_score(real, prediccion)
    mae = mean_absolute_error(real, prediccion)
    mse = mean_squared_error(real, prediccion)
    #msle = mean_squared_log_error(real, prediccion)
    mdae = median_absolute_error(real, prediccion)
    r2 = r2_score(real, prediccion)
    rmse = sqrt(mse)
    data = np.array([varianza, mae, mse, mdae, r2, rmse ])
    return data

def mean_absolute_percentage_error(real, prediccion):
    y_true, y_pred = np.array(real), np.array(prediccion)
    mask = y_true != 0
    return np.mean(np.fabs((y_true[mask] - y_pred[mask]) / y_true[mask] )) * 100
