import matplotlib.pyplot as plt
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from matplotlib.figure import Figure
from matplotlib.backends.backend_gtk3agg import FigureCanvas
from matplotlib.backends.backend_gtk3 import (NavigationToolbar2GTK3 as NavigationToolbar)

# Plot graph: predicted VS actual
def graficar_prediccion(data_real, data_proyectada):
    win = Gtk.Window()
    win.connect("destroy", lambda x: Gtk.main_quit())
    win.set_default_size(400,300)
    win.set_title("Embedding in GTK")
    vbox = Gtk.VBox()
    win.add(vbox)

    fig = Figure(figsize=(5,4), dpi=100)
    ax = fig.add_subplot(111)
    ax.plot(data_proyectada, label='Proyectado')
    ax.plot(data_real, label='Actual')
    ax.legend()

    canvas = FigureCanvas(fig)  # a Gtk.DrawingArea
    vbox.pack_start(canvas, True, True, 0)
    toolbar = NavigationToolbar(canvas, win)
    vbox.pack_start(toolbar, False, False, 0)
    win.show_all()
    Gtk.main()

#renderizado de la perdida de datos
def graficar_perdida_datos(history):
    plt.figure(figsize=(10,8))
    plt.plot(history.history['mean_absolute_error'],  label = 'Perdida entrenamiento')
    plt.plot(history.history['val_mean_absolute_error'],  label = 'Perdida validacion')
    plt.ylabel('Perdida')
    plt.xlabel('Iteraciones')
    plt.legend()

#renderizado de metricas
def graficar_metricas(history):
    plt.figure(figsize=(10,8))
    plt.plot(history.history['mean_squared_error'], label = 'mse')
    plt.plot(history.history['mean_absolute_error'], label = 'mae')
    plt.ylabel('Perdida')
    plt.xlabel('Iteraciones')
    plt.legend()

# renderiza una serie de tiempo
def graficar_serie_tiempo(data):
    plt.figure(figsize=(10,8))
    plt.plot(data, label = 'Real')
    plt.ylabel('Cantidad de actividades')
    plt.xlabel('Dias')
    plt.legend()

# renderiza las series de tiempo real y pronosticada
def graficar_comparativo_series_tiempo(data_real, data_pred, escala):
    real = escala.inverse_transform(data_real)
    pred = escala.inverse_transform(data_pred)
    plt.figure(figsize=(10,8))
    plt.plot(pred, label = 'Predicción')
    plt.plot(real, label = 'Real')
    plt.ylabel('Cantidad de actividades')
    plt.xlabel('Dias')
    plt.legend()

# renderiza el histograma con el error residual
def graficar_histograma_perdida(y_test, prediction):
    error =  y_test - prediction # residual error
    plt.figure(figsize=(10,8))
    plt.hist(error, bins=50, label = 'error residual')
    plt.xlabel("Error de prediccion")
    plt.ylabel("Cantidad")
    plt.legend()
