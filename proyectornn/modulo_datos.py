import pandas as pd
import requests as rq
import json
import numpy as np
from sklearn.preprocessing import RobustScaler, PowerTransformer, QuantileTransformer, MinMaxScaler

# Funcion para construir el set de datosa partir de un request
def input_datos_request(url):
    r = rq.get(url)
    x = r.json()
    tipos = {'cantidad_servicios': 'float','cantidad_tecnicos': 'float'}
    df = pd.read_json(json.dumps(x),convert_dates=['fecha'],dtype=tipos)
    df = df.set_index('fecha')
    return df

# Funcion para construir el set de datos a partir de un archivo json
def input_datos_json(ruta_archivo):
    with open(ruta_archivo) as f:
        data = json.load(f)
    df = pd.read_json(json.dumps(data), convert_dates=['fecha'], orient='records')
    df = df.set_index('fecha')
    return df

# Funcion para limpiar los valores atipicos del set de datos
def limpiar_outliers(df):
    filtered = None
    rango = list(range(df.index.min().year, df.index.max().year+1))
    for x in rango:
        Q1, Q3 = df[str(x)].cantidad_servicios.quantile([0.25,0.75])
        IQR = Q3 - Q1
        datatemp = df[str(x)].query('(@Q1 - 1.5 * @IQR) <= cantidad_servicios <= (@Q3 + 1.5 * @IQR)')
        filtered = pd.concat([filtered,datatemp])
    return filtered

# Funcion para construir la matriz de ventana deslizante
def crear_ventana(data, ancho_ventana, time_step):
    raw = []
    ventana_ajustada = ancho_ventana+time_step
    for index in range(len(data) - ventana_ajustada):
    		raw.append(data[index: index + ventana_ajustada])
    raw = np.array(raw)
    return raw

# Funcion que encapsula los metodos de transformacion de datos
def normalizar_datos(escalador, data):
    switcher={
    'robust': RobustScaler(),
    'power': PowerTransformer(),
    'quantile': QuantileTransformer(),
    'minmax': MinMaxScaler(),
    }
    escala = switcher.get(escalador,'quantile')
    escala = escala.fit(data)
    data_escalada = escala.transform(data)
    return data_escalada, escala

# Funcion para invetir el escalado de los datos sin valores atipicos
def invertir_normalizado(escala, data):
    data_invertida = escala.inverse_transform(data)
    return data_invertida

# Funcion para generar los set de datos de entrenamiento
def dividir_datos_entrenamiento(data_escalada, split_ratio):
    data_entrenamiento = []
    data_entrenamiento = data_escalada[:int(split_ratio), :]
    #np.random.shuffle(data_entrenamiento)
    x_train = data_entrenamiento[:, :-1]
    y_train = data_entrenamiento[:, -1]
    return x_train, y_train

# Funcion para generar los set de datos de pruebas
def dividir_datos_validacion(data_escalada, split_ratio):
    data_validacion = []
    data_validacion = data_escalada[int(split_ratio):, :]
    x_test = data_validacion[:, :-1]
    y_test = data_validacion[:, -1]
    return x_test, y_test

#lote de datos para metricas
def dividir_datos(data_escalada):
    x_test = data_escalada[:, :-1]
    y_test = data_escalada[:, -1]
    return x_test, y_test
