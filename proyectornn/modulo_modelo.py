from tensorflow.keras.layers import Dense, Activation, Dropout
from tensorflow.keras.layers import LSTM
from tensorflow.keras.models import Sequential
from tensorflow.keras.callbacks import TensorBoard
from time import time

# Configura el modelo de prediccion
def construir_modelo(input_time_step, input_dim, hidden_unit, dropout):
    model = Sequential()
    model.add(LSTM(1,input_shape=(input_time_step, input_dim), return_sequences=True))
    model.add(Dropout(dropout))
    #model.add(LSTM(hidden_unit, batch_input_shape=(1, input_time_step, input_dim), return_sequences=True, stateful=True))
    model.add(LSTM(hidden_unit, return_sequences=False))
    model.add(Dropout(dropout))
    model.add(Dense(1))
    model.add(Activation("tanh"))
    #model.add(Activation("relu"))
    model.compile(loss="mse", optimizer="rmsprop", metrics=['mae', 'mape'])
    return model

# Entrena el modelo predictivo
def entrenar_modelo(model, data_x, data_y, batch_size, epochs):
    tensorboard = TensorBoard(log_dir='logs/{}'.format(time()))
    history = model.fit(data_x, data_y, batch_size=batch_size, epochs=epochs, callbacks=[tensorboard])
    return history, model

# Entrena el modelo para elaboracion de metricas
def entrenar_modelo_metricas(model, data_x, data_y, batch_size, validation, epochs):
    tensorboard = TensorBoard(log_dir='logs/{}'.format(time()))
    history = model.fit(data_x, data_y, batch_size=batch_size, epochs=epochs, validation_split=validation, callbacks=[tensorboard])
    return history, model
