from proyectornn import modulo_datos as md
from proyectornn import modulo_modelo as mm
from proyectornn import modulo_graficas as mg
from proyectornn import modulo_evaluacion as me
import numpy as np

#Importacion de datos
#data = md.input_datos_request('http://192.168.0.17:54286/api/DataTraining?Filtro=DatosPorDias&FechaIni=2016-01-01&FechaFin=2017-12-31')
data = md.input_datos_json('./proyectornn/data.json')

# limpieza de valores atipicos
data = md.limpiar_outliers(data)
data = data.cantidad_servicios.values
mg.graficar_serie_tiempo(data)

#Transormacion de vectores a matriz de datos
matrix = np.reshape(data, (data.shape[0], 1))

# Escalado de datos (normalizacion)
data_normalizada, escala = md.normalizar_datos('quantile', matrix)

# Construye la ventana de datos
window_size = 14
raw = md.crear_ventana(data_normalizada, window_size,1)

# division de los datos en los lotes de entrenamieto y validacion
split_ratio = round(0.8 * raw.shape[0]) # rango de corte
x_train, y_train = md.dividir_datos_entrenamiento(raw, split_ratio)
x_test, y_test = md.dividir_datos_validacion(raw, split_ratio)

#transformacion de matriz a cubos de datos
x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))
x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))

# Construccion del modelo predictivo (input_time_step, input_dim, hidden_unit, dropout)
hidden_unit = 50
dropout =  0.2
model = mm.construir_modelo(x_train.shape[1], x_train.shape[2], hidden_unit, dropout)

# Entrenamiento del modelo con el set de datos destinado para este fin
batch_size = 2
epochs = 50
history, model = mm.entrenar_modelo(model, x_train, y_train, batch_size, epochs)

# Prediccion
prediction2 = history.model.predict(x_test)
prediction = model.predict(x_test)

####################################################################################################
# varianza, mae, mse, mdae, r2, rmse
me.generar_metricas(y_test, prediction)

# mape
me.mean_absolute_percentage_error(y_test, prediction)

# mse, mae, mape
model.evaluate(x_test, y_test, verbose=0)

####################################################################################################

mg.graficar_comparativo_series_tiempo(y_test, prediction, escala)

mg.graficar_histograma_perdida(y_test, prediction)
