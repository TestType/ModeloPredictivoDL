from proyectornn import modulo_datos as md
from sklearn.preprocessing import RobustScaler
import numpy as np
from tensorflow.keras.layers import Dense, Activation, Dropout
from tensorflow.keras.layers import LSTM
from tensorflow.keras.models import Sequential
import matplotlib.pyplot as plt

df = md.construir_dataset_json('./proyectornn/data.json')


raw = []
window_size = 6
adjusted_window = 7
hidden_unit = 100
data = df.cantidad_servicios.values
data = data.reshape(data.shape[0],1)
print(data)
# Escalado de datos (normalizacion)
transformer = RobustScaler().fit(data)
data_escalada = transformer.transform(data)
print(data_escalada)
data_reinvertida = transformer.inverse_transform(data_escalada)
print(data_reinvertida)

data_reinvertida.shape
data_escalada.shape

# Split data into windows
for index in range(len(data) - adjusted_window):
		raw.append(data[index: index + adjusted_window])

raw.shape

x = raw[:, :-1]
y = raw[:, -1, [0]]

x2 = raw[:-1]
y2 = raw[-1, [0]]

print(raw[0])

print(x[0])
print(y2)

x.shape
y.shape

x2.shape
y2.shape

data_escalada.shape[0]
round(0.8 * data_escalada.shape[0])

# rango de corte
split_ratio = round(0.8 * data_escalada.shape[0])

# Split the input dataset into train and test
raw = np.array(raw)
data_entrenamiento = data_escalada[:int(split_ratio), :]
data_validacion = data_escalada[int(split_ratio):, :]
data_validacion_org = raw[int(split_ratio):, :]

data_escalada.shape
data_entrenamiento.shape
data_validacion.shape

data_entrenamiento
data_validacion

np.random.shuffle(data_entrenamiento)

# x_train and y_train, for training
x_train = data_entrenamiento[:, :-1]
y_train = data_entrenamiento[:, -1]

# x_test and y_test, for testing
x_test = data_validacion[:, :-1]
y_test = data_validacion[:, -1]

#redimension a cubos
x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))
x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))

# x_test and y_test, for testing data org
x_test_raw = data_validacion_org[:, :-1]
y_test_raw = data_validacion_org[:, -1]


# Build RNN (LSTM) model
lstm_layer = [1, window_size, hidden_unit, 1]
dropout_keep_prob = 0.2

model = Sequential()
model.add(LSTM(1,input_shape=(lstm_layer[1], lstm_layer[0]), return_sequences=True))
model.add(Dropout(dropout_keep_prob))
model.add(LSTM(lstm_layer[2], return_sequences=False))
model.add(Dropout(dropout_keep_prob))
model.add(Dense(1))
model.add(Activation("tanh"))
model.compile(loss="mean_squared_error", optimizer="rmsprop")

# Train RNN (LSTM) model with train set
batch_size = 2
epochs = 100
validation_split = 0.1
model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_split=validation_split)

# Check the model against test set
prediction = model.predict(x_test)
predicted = np.reshape(prediction, (prediction.size,))
predicted_raw = []
for i in range(len(x_test_raw)):
	predicted_raw.append((predicted[i] + 1) * x_test_raw[i][0])

# Plot graph: predicted VS actual
plt.subplot(111)
plt.plot(predicted_raw, label='Predicted')
plt.plot(y_test_raw, label='Actual')
plt.legend()
plt.show()
