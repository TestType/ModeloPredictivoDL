import pandas as pd
import requests as rq
import json
import numpy as np
import matplotlib.pyplot as plt


df = pd.read_json(x,convert_dates=['fecha'])
df = df.set_index('fecha')

df.head()
df4 = pd.pivot_table(df,values='cantidad',index='fecha',columns='oferta')
df4.tail()
df.dtypes
df.shape
len(df)
# conteo de datos
df.count()
df.describe()
df.info()
# validacion de elementos nulos
df.isnull().any()
df.plot(figsize=(10, 8))
plt.show()
df.loc[:,['cantidad_tecnicos']].head()
df.iloc[:,1:2].head()
df.loc[:,['cantidad_servicios']].head()
df.iloc[:,0:1].head()
df['2012'].plot()
df['2013'].plot()
df.query('cantidad < 400')
df.hist()

# Funcion para construir el data frame con el set de datos
def construir_dataset(url):
    r = rq.get(url)
    x = r.json()
    tipos = {'cantidad_servicios': 'float','cantidad_tecnicos': 'float'}
    df = pd.read_json(json.dumps(x),convert_dates=['fecha'],dtype=tipos)
    df = df.set_index('fecha')
    return df

# Funcion para limpiar los valores atipicos del set de datos
def outlier_iqr(df):
    filtered = None
    rango = list(range(df.index.min().year, df.index.max().year+1))
    for x in rango:
        Q1, Q3 = df[str(x)].cantidad_servicios.quantile([0.25,0.75])
        IQR = Q3 - Q1
        datatemp = df[str(x)].query('(@Q1 - 1.5 * @IQR) <= cantidad_servicios <= (@Q3 + 1.5 * @IQR)')
        filtered = pd.concat([filtered,datatemp])
    return filtered

def analizar_datos(df):
    df.head()
    df.tail()
    df.dtypes
    df.shape
    len(df)
    df.count()
    df.describe()
    df.info()
    df.hist()
    df.plot()
    df.boxplot()

df = construir_dataset('http://localhost:54286/api/DataTraining?Filtro=DatosPorDias&FechaIni=2016-01-01&FechaFin=2017-12-31')

df = df.round(decimals=1)
df.head()

frameCorregido = outlier_iqr(df)
df.plot()
frameCorregido.plot()

df.describe()
frameCorregido.describe()

# Computing IQR
Q1, Q3 = df['2012'].cantidad_servicios.quantile([0.25,0.75])
IQR = Q3 - Q1
filtered = df['2012'].query('(@Q1 - 1.5 * @IQR) <= cantidad_servicios <= (@Q3 + 1.5 * @IQR)')

len(df['2012'])
len(filtered)

df['2012'].plot()

## filtered.plot()
df['2012'].boxplot(figsize=(10, 8))
df['2012'].join(filtered, rsuffix='_filtered').boxplot()
df['2012'] = filtered
df.plot()
df.query('cantidad_tecnicos == 0')
df.isnull().any()
data = list(range(df.index.min().year, df.index.max().year+1))
print(data)
for x in data:
    print(str(x))
df[str(2012)].plot()
print (df.index.min().year)
print (df.index.max().year+1)
