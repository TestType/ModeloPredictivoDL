import pandas as pd
import requests as rq
import json
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as ss

def construir_dataset(url):
    r = rq.get(url)
    x = r.json()
    df = pd.read_json(json.dumps(x),convert_dates=['fecha'])
    return df

df = construir_dataset('http://localhost:54286/api/DataTraining?Filtro=DatosPorDias&FechaIni=2013-01-01&FechaFin=2017-12-31')

# Dimensiones del dataset
df.shape

# Nombre de las columnas
df.columns
# Tipos de datos del dataset
df.dtypes

# Verificar registros por columna y valores nulos
df.info()

# Estadisticos basicos
# media
df.mean()

# mediana
df.median()

# Moda
df.cantidad_servicios.mode()
df.cantidad_tecnicos.mode()

#Consolidado
df.describe()

df.fecha.describe(include='all')



# Varianza y desviacion estandar
df.std(ddof=0)
df.var(ddof=0)


# rango entre el maximo y el minimo y el rango intercuartilico
rango = df.max() - df.min()
iqr = df.quantile(0.75) - df.quantile(0.25)

print(rango)
print(iqr)



df.query('cantidad_servicios < 100 ').agg(['count'])
df.cantidad_servicios.plot(kind='kde')
df.cantidad_tecnicos.plot(kind='kde',color='g')
plt.xlabel('eje x')
plt.ylabel('eje y')
plt.show()


# Coeficiente de variacion que sirve para comparar entre dos muestras, cuál varía más y cuál es más estable.
cv = df.std(ddof=0) / df.mean()
cv2 = ss.variation(df.cantidad_servicios)
assert(cv.cantidad_servicios == cv2)

print(cv)
print(cv2)


# correlaciones

def calc_covariance(x, y):
    x_mean = x.mean()
    y_mean = y.mean()
    x_diff = [nx - x_mean for nx in x]
    y_diff = [ny - y_mean for ny in y]
    xy_mult = [ (x_diff[i] * y_diff[i]) for i in range(0,len(x_diff))]
    covariance = sum(xy_mult) / len(x)
    return covariance

def calc_correlation(x, y):
    covariance = calc_covariance(x, y)
    correlation = covariance / ((x.var(ddof=0) ** (1/2)) * (y.var(ddof=0) ** (1/2)))
    return correlation

corr = calc_correlation(df.cantidad_servicios,df.cantidad_tecnicos)

print(corr)
df.cov()
correlation = df.corr(method='pearson')
print(correlation)

plt.scatter(df.cantidad_servicios,df.cantidad_tecnicos,label='Correlacion')
plt.legend()

df.plot(kind='scatter',x='cantidad_servicios',y='cantidad_tecnicos',label='Test')

# Medida de asimetria sobre la media (indican si es o no una distribucion normal)
#Kurtosis= los datos estan muy concentrados en la media, forma apuntada
#Asimetria Asimetría negativa: la cola de la distribución se alarga para valores inferiores a la media

asimetria = ss.skew(df)
kurtosis = ss.kurtosis(df)
print(asimetria)
print(kurtosis)







df.hist()

df.boxplot()
df.boxplot(return_type='axes')
df = df.set_index('fecha')
df.plot()
