## From SQL to DataFrame Pandas
## https://tomaztsql.wordpress.com/2018/07/15/using-python-pandas-dataframe-to-read-and-insert-data-to-microsoft-sql-server/

import pandas as pd
import pyodbc
import matplotlib.pyplot as plt

sql_conn = pyodbc.connect("""DRIVER={ODBC Driver 13 for SQL Server};
SERVER=DESKTOP-54O5295;
DATABASE=ADSLplus;
Trusted_Connection=yes""")

#query = "SELECT * FROM ADSLPLUS_CAMIONETAS"
#df = pd.read_sql(query, sql_conn)
#df.head(3)

query = """select convert(date,dateadd(ww, datediff(ww, 0, FechaCierre), 0)) [FechaCierre], COUNT(0) [Cantidad]
from ADSLPLUS_LINEABASICA
where FechaCierre between  CONVERT(date,'2015-01-01') and  CONVERT(date,'2017-12-31')
group by dateadd(ww, datediff(ww, 0, FechaCierre), 0)
order by dateadd(ww, datediff(ww, 0, FechaCierre), 0)
"""

df = pd.read_sql(query, sql_conn,index_col="FechaCierre", parse_dates=["FechaCierre"])
df.head()
df.dtypes
df.count()
df.describe()
#df.plot(figsize=(10, 8))
plt.figure()
df.plot()

plot = df['2017'].plot(label='test',figsize=(10, 8),title='Test')
